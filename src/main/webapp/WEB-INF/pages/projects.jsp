<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!doctype html>
<html lang="en">
<head>
<%@ include file="template/header.jsp"%>
</head>

<body>
	<!-- banner -->
	<div class="banner_inner_agileits">
		<div class="container">
			<nav class="navbar navbar-default">
				<div class="navbar-header navbar-left">
					<button type="button" class="navbar-toggle collapsed"
						data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
						<span class="sr-only">Toggle navigation</span> <span
							class="icon-bar"></span> <span class="icon-bar"></span> <span
							class="icon-bar"></span>
					</button>
					<h1>
						<a class="navbar-brand" href="index.html"><span>R</span>adiant
							Energy</a>
					</h1>
				</div>
				<!-- Collect the nav links, forms, and other content for toggling -->
				<div class="agileits_w3layouts_sign_in">
					<a href="solarquote" class="play-icon popup-with-zoom-anim">Solar Quotes</a>

				</div>
				<div class="collapse navbar-collapse navbar-right"
					id="bs-example-navbar-collapse-1">
					<nav class="menu--puck">
						<ul class="menu__list">
							<li class="menu__item"><a href="index" class="menu__link">Home</a></li>
							<li class="menu__item"><a href="about" class="menu__link">About</a></li>
							<li class="menu__item menu__item--current"><a
								href="projects" class="menu__link">Projects</a></li>
							<li class="menu__item"><a href="contact" class="menu__link">Contact</a></li>
						</ul>
					</nav>

				</div>
				<div class="clearfix"></div>
			</nav>
			<div class="w3layouts_banner_info_inner">
				<h2>
					Pro<span>jects</span>
				</h2>
				<p>Our success stories</p>
			</div>
		</div>
	</div>
	<!-- //banner -->
	<!--/projects -->
	<div class="agile_services">
		<h3 class="wthree_text_info two">
			Our <span>Projects</span>
		</h3>
		<div class="container">
			<div class="col-md-4 agileinfo_portfolio_grid project">
				<a href="${g1}" class="lsb-preview" data-lsb-group="header">
					<div class="agileits_portfolio_grid project_icon">
						<img src="${g1}" alt=" " class="img-responsive" />
					</div>
				</a>
			</div>
			<div class="col-md-4 agileinfo_portfolio_grid project">
				<a href="${g2}" class="lsb-preview" data-lsb-group="header">
					<div class="agileits_portfolio_grid project_icon">
						<img src="${g2}" alt=" " class="img-responsive" />
					</div>
				</a>
			</div>
			<div class="col-md-4 agileinfo_portfolio_grid project">
				<a href="${g3}" class="lsb-preview" data-lsb-group="header">
					<div class="agileits_portfolio_grid project_icon">
						<img src="i${g3}" alt=" " class="img-responsive" />
					</div>
				</a>
			</div>
			<div class="col-md-4 agileinfo_portfolio_grid project">
				<a href="${g4}" class="lsb-preview" data-lsb-group="header">
					<div class="agileits_portfolio_grid project_icon">
						<img src="${g4}" alt=" " class="img-responsive" />
					</div>
				</a>
			</div>
			<div class="col-md-4 agileinfo_portfolio_grid project">
				<a href="${g5}" class="lsb-preview" data-lsb-group="header">
					<div class="agileits_portfolio_grid project_icon">
						<img src="${g5}" alt=" " class="img-responsive" />
					</div>
				</a>
			</div>
			<div class="col-md-4 agileinfo_portfolio_grid project">
				<a href="${g1}" class="lsb-preview" data-lsb-group="header">
					<div class="agileits_portfolio_grid project_icon">
						<img src="${g1}" alt=" " class="img-responsive" />
					</div>
				</a>
			</div>
			<div class="col-md-4 agileinfo_portfolio_grid project">
				<a href="${g5}" class="lsb-preview" data-lsb-group="header">
					<div class="agileits_portfolio_grid project_icon">
						<img src="${g5}" alt=" " class="img-responsive" />
					</div>
				</a>
			</div>
			<div class="col-md-4 agileinfo_portfolio_grid project">
				<a href="${g2}" class="lsb-preview" data-lsb-group="header">
					<div class="agileits_portfolio_grid project_icon">
						<img src="${g2}" alt=" " class="img-responsive" />
					</div>
				</a>
			</div>
			<div class="col-md-4 agileinfo_portfolio_grid project">
				<a href="${g3}" class="lsb-preview" data-lsb-group="header">
					<div class="agileits_portfolio_grid project_icon">
						<img src="${g3}" alt=" " class="img-responsive" />
					</div>
				</a>
			</div>
			<div class="clearfix"></div>
		</div>
	</div>
	<!--//projects -->
</body>

<footer>
	<%@ include file="template/footer.jsp"%>
</footer>

</html>

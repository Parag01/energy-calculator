<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!doctype html>
<html lang="en">
<head>
<%@ include file="template/header.jsp"%>
</head>

<body>
	<!-- banner -->
	<div class="banner_inner_agileits">
		<div class="container">
			<nav class="navbar navbar-default">
				<div class="navbar-header navbar-left">
					<button type="button" class="navbar-toggle collapsed"
						data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
						<span class="sr-only">Toggle navigation</span> <span
							class="icon-bar"></span> <span class="icon-bar"></span> <span
							class="icon-bar"></span>
					</button>
					<h1>
						<a class="navbar-brand" href="index.html"><span>R</span>adiant
							Energy</a>
					</h1>
				</div>
				<!-- Collect the nav links, forms, and other content for toggling -->
				<div class="agileits_w3layouts_sign_in">
					<a href="solarquote" class="play-icon popup-with-zoom-anim">Solar
						Quotes</a>

				</div>
				<div class="collapse navbar-collapse navbar-right"
					id="bs-example-navbar-collapse-1">
					<nav class="menu--puck">
						<ul class="menu__list">
							<li class="menu__item"><a href="index" class="menu__link">Home</a></li>
							<li class="menu__item"><a href="about" class="menu__link">About</a></li>
							<li class="menu__item"><a href="projects" class="menu__link">Projects</a></li>
							<li class="menu__item menu__item--current"><a href="contact"
								class="menu__link">Contact</a></li>
						</ul>
					</nav>

				</div>
				<div class="clearfix"></div>
			</nav>
			<div class="w3layouts_banner_info_inner">
				<h2>
					Contact <span>Us</span>
				</h2>
				<p>We're ready to help you with your solar power query</p>
			</div>
		</div>
	</div>
	<!-- //banner -->
	<!-- mail -->
	<div class="mail" id="mail">
		<div class="container">
			<h3 class="wthree_text_info two">
				Contact <span>Us</span>
			</h3>
			<div class="agile_banner_bottom_grids">
				<div class="col-md-8 agile_map">
					<iframe
						src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d387142.84010033106!2d-74.25819252532891!3d40.70583163828471!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x89c24fa5d33f083b%3A0xc80b8f06e177fe62!2sNew+York%2C+NY%2C+USA!5e0!3m2!1sen!2sin!4v1475140387172"
						style="border: 0"></iframe>
				</div>
				<div class="col-md-4 w3_agile_contact_grids_info">
					<div class="w3_agile_contact_grid">
						<div class="agile_contact_grid_right agilew3_contact">
							<h4>OFFICE 1</h4>
							<p>435 City hall,</p>
							<p>NewYork City SD092.</p>
						</div>
					</div>
					<div class="w3_agile_contact_grid">
						<div class="agile_contact_grid_right agileits_w3layouts_right">
							<h4>OFFICE 2</h4>
							<p>088 Jasmine hall,</p>
							<p>NewYork City SD092.</p>
						</div>
					</div>
					<div class="w3_agile_contact_grid" data-aos="flip-up">

						<div class="agile_contact_grid_right agileits_w3layouts_right1">
							<h4>OFFICE 3</h4>
							<p>436 Honey hall,</p>
							<p>NewYork City SD092.</p>
						</div>
						<div class="clearfix"></div>
					</div>
					<div class="clearfix"></div>
				</div>
				<div class="clearfix"></div>
			</div>
			<div class="w3_agile_form">
				<h3 class="wthree_text_info three">
					Send Us <span>a Message</span>
				</h3>
				<form action="#" method="post">
					<span class="input input--chisato"> <input
						class="input__field input__field--chisato" name="Name" type="text"
						id="input-13" placeholder=" " required="" /> <label
						class="input__label input__label--chisato" for="input-13">
							<span class="input__label-content input__label-content--chisato"
							data-content="Name">Name</span>
					</label>
					</span> <span class="input input--chisato"> <input
						class="input__field input__field--chisato" name="Email"
						type="email" id="input-14" placeholder=" " required="" /> <label
						class="input__label input__label--chisato" for="input-14">
							<span class="input__label-content input__label-content--chisato"
							data-content="Email">Email</span>
					</label>
					</span> <span class="input input--chisato"> <input
						class="input__field input__field--chisato" name="Subject"
						type="text" id="input-15" placeholder=" " required="" /> <label
						class="input__label input__label--chisato" for="input-15">
							<span class="input__label-content input__label-content--chisato"
							data-content="Subject">Subject</span>
					</label>
					</span>
					<textarea name="Message" placeholder="Your comment here..."
						required=""></textarea>
					<input type="submit" value="Submit">
				</form>


			</div>
			<div class="clearfix"></div>
		</div>
	</div>
	<!-- //mail -->
</body>

<footer>
	<%@ include file="template/footer.jsp"%>
</footer>

</html>

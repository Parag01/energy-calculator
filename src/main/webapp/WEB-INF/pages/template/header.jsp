<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!doctype html>
<html lang="en">

<head>

<!-- Including CSS as Resources-->
<spring:url value="/resources/css/bootstrap.css" var="bootstrapCSS" />
<spring:url value="/resources/css/popuo-box.css" var="popuoBoxCSS" />
<spring:url value="/resources/css/owl.carousel.css" var="owlCarouselCSS" />
<spring:url value="/resources/css/style.css" var="styleCSS" />
<spring:url value="/resources/css/font-awesome.css" var="fontAwesomeCSS" />

<spring:url value="/resources/css/demo.css" var="demoCSS" />
<spring:url value="/resources/css/material-bootstrap-wizard.css"
	var="materialBootstrapCSS" />

<!-- Including Images as Resources -->
<spring:url value="/resources/img/banner1.jpg" var="banner1" />
<spring:url value="/resources/img/banner.jpg" var="banner" />

<spring:url value="/resources/img/g1.jpg" var="g1" />
<spring:url value="/resources/img/g2.jpg" var="g2" />
<spring:url value="/resources/img/g3.jpg" var="g3" />
<spring:url value="/resources/img/g4.jpg" var="g4" />
<spring:url value="/resources/img/g5.jpg" var="g5" />
<spring:url value="/resources/img/g6.jpg" var="g6" />

<spring:url value="/resources/img/1.png" var="g7" />
<spring:url value="/resources/img/2.png" var="g8" />
<spring:url value="/resources/img/3.png" var="g9" />
<spring:url value="/resources/img/4.png" var="g10" />

<spring:url value="/resources/img/bar.jpg" var="bar" />
<spring:url value="/resources/img/tick.png" var="tick" />

<title>Radiant Energy an Industrial Category Bootstrap
	Responsive Website Template | Home</title>
<!-- for-meta-tags -->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords"
	content="Radiant Energy Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
<!-- //for-meta-tags -->

<link href="${bootstrapCSS}" rel="stylesheet" type="text/css"
	media="all" />

<!-- pop-up-box -->
<link href="${popuoBoxCSS}" rel="stylesheet" type="text/css" media="all" />
<!-- //pop-up-box -->

<link rel="stylesheet" href="${owlCarouselCSS}" type="text/css"
	media="all">

<link href="${styleCSS}" rel="stylesheet" type="text/css" media="all" />

<!-- font-awesome icons -->
<link rel="stylesheet" href="${fontAwesomeCSS}" />
<!-- //font-awesome icons -->



<!-- bootstrap min -->
<link href="${materialBootstrapCSS}" rel="stylesheet" />
<!-- //bootstrap min -->

<!-- demo -->
<link href="${demo}" rel="stylesheet" />
<!-- //demo -->

<link
	href="//fonts.googleapis.com/css?family=Cabin+Condensed:400,500,600,700"
	rel="stylesheet">
<link
	href='//fonts.googleapis.com/css?family=Open+Sans:100,400,300,300italic,400italic,600,600italic,700,700italic,800,800italic'
	rel='stylesheet' type='text/css'>

<link rel="stylesheet" type="text/css"
	href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" />


<link href="//cdn.jsdelivr.net/chartist.js/latest/chartist.min.css"
	rel="stylesheet" type="text/css" />

</head>
</html>

<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!doctype html>
<html lang="en">

<footer>
	<div class="footer">
		<div class="container">
			<div class="col-md-3 w3agile_footer_grid">
				<h3>About Us</h3>
				<p>Itaque earum rerum hic tenetur a sapiente delectus, ut aut
					reiciendis voluptatibus.</p>

			</div>
			<div class="col-md-2 w3agile_footer_grid">
				<h3>Links</h3>
				<ul>
					<li><a href="index.html"><i
							class="fa fa-angle-double-right" aria-hidden="true"></i> Home</a></li>
					<li><a href="projects.html"><i
							class="fa fa-angle-double-right" aria-hidden="true"></i> Projects</a></li>
					<li><a href="about.html"><i
							class="fa fa-angle-double-right" aria-hidden="true"></i> About</a></li>
					<li><a href="contact.html"><i
							class="fa fa-angle-double-right" aria-hidden="true"></i>Contact
							Us</a></li>
				</ul>
			</div>
			<div class="col-md-4 w3agile_footer_grid">
				<h3>Twitter Posts</h3>
				<ul class="w3agile_footer_grid_list">
					<li>Ut aut reiciendis voluptatibus maiores alias, ut aut
						reiciendis. <span><i class="fa fa-twitter"
							aria-hidden="true"></i> 02 days ago</span>
					</li>
					<li>Itaque earum rerum hic tenetur a sapiente delectus
						voluptatibus. <span><i class="fa fa-twitter"
							aria-hidden="true"></i> 03 days ago</span>
					</li>
				</ul>
			</div>
			<div class="col-md-3 w3agile_footer_grid">
				<h3>Newsletter</h3>
				<p>Sign Up for our Newsletter and follow us on social media</p>
				<form action="#" method="post">
					<input type="email" name="Email" placeholder="Enter Your Email..."
						required=""> <input type="submit" value="subscribe">
				</form>

			</div>
		</div>
	</div>
	<div class="agileinfo_copy_right">
		<div class="container">
			<div class="agileinfo_copy_right_left">
				<p>
					� 2017 Radiant Energy. All rights reserved | Design by <a
						href="http://w3layouts.com/">W3layouts</a>
				</p>
			</div>
			<div class="agileinfo_copy_right_right">
				<ul class="social">
					<li><a class="social-linkedin" href="#"> <i
							class="fa fa-facebook" aria-hidden="true"></i>
							<div class="tooltip">
								<span>Facebook</span>
							</div>
					</a></li>
					<li><a class="social-twitter" href="#"> <i
							class="fa fa-twitter" aria-hidden="true"></i>
							<div class="tooltip">
								<span>Twitter</span>
							</div>
					</a></li>
					<li><a class="social-google" href="#"> <i
							class="fa fa-google" aria-hidden="true"></i>
							<div class="tooltip">
								<span>Google+</span>
							</div>
					</a></li>
					<li><a class="social-facebook" href="#"> <i
							class="fa fa-pinterest-p" aria-hidden="true"></i>
							<div class="tooltip">
								<span>Pinterest</span>
							</div>
					</a></li>
					<li><a class="social-instagram" href="#"> <i
							class="fa fa-instagram" aria-hidden="true"></i>
							<div class="tooltip">
								<span>Instagram</span>
							</div>
					</a></li>
				</ul>
			</div>
			<div class="clearfix"></div>
		</div>
	</div>
</footer>

<spring:url value="/resources/js/jquery-2.2.4.min.js" var="jqueryJS" />
<spring:url value="/resources/js/bootstrap.min.js" var="bootstrapMinJS" />
<spring:url value="/resources/js/jquery.bootstrap.js"
	var="jqueryBootstrapJS" />
<spring:url value="/resources/js/material-bootstrap-wizard.js"
	var="materialBootstrapJS" />
<spring:url value="/resources/js/jquery.validate.min.js"
	var="jqueryValidateJS" />
<spring:url value="" var="chartistMinJS" />
<spring:url value="/resources/js/chartist.js" var="chartistJS" />

<!--   Core JS Files   -->
<script src="${jqueryJS}" type="text/javascript"></script>
<script src="${bootstrapMinJS}" type="text/javascript"></script>
<script src="${jqueryBootstrapJS}" type="text/javascript"></script>

<!--  Plugin for the Wizard -->
<script src="${materialBootstrapJS}"></script>

<!--  More information about jquery.validate here: http://jqueryvalidation.org/	 -->
<script src="${jqueryValidateJS}"></script>

<script src="//cdn.jsdelivr.net/chartist.js/latest/chartist.min.js"></script>
<script src="${chartistJS}"></script>

</html>

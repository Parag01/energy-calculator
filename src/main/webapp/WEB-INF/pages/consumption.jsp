<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!doctype html>
<html lang="en">
<head>
<%@ include file="template/header.jsp"%>
</head>

<body>
	<!-- banner -->
	<div class="banner_inner_agileits">
		<div class="container">
			<nav class="navbar navbar-default">
				<div class="navbar-header navbar-left">
					<button type="button" class="navbar-toggle collapsed"
						data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
						<span class="sr-only">Toggle navigation</span> <span
							class="icon-bar"></span> <span class="icon-bar"></span> <span
							class="icon-bar"></span>
					</button>
					<h1>
						<a class="navbar-brand" href="index.html"><span>R</span>adiant
							Energy</a>
					</h1>
				</div>
				<!-- Collect the nav links, forms, and other content for toggling -->
				<div class="agileits_w3layouts_sign_in">
					<a href="solarquote" class="play-icon popup-with-zoom-anim">Solar
						Quotes</a>

				</div>
				<div class="collapse navbar-collapse navbar-right"
					id="bs-example-navbar-collapse-1">
					<nav class="menu--puck">
						<ul class="menu__list">
							<li class="menu__item"><a href="index" class="menu__link">Home</a></li>
							<li class="menu__item"><a href="about" class="menu__link">About</a></li>
							<li class="menu__item"><a href="projects" class="menu__link">Projects</a></li>
							<li class="menu__item"><a href="contact" class="menu__link">Contact</a></li>
						</ul>
					</nav>

				</div>
				<div class="clearfix"></div>
			</nav>
			<div class="w3layouts_banner_info_inner">
				<h2>
					Solar <span>Quote</span>
				</h2>
				<p>Calculate the Solar Energy Consumption</p>
			</div>
		</div>
	</div>
	<!-- //banner -->
	<!-- calculations -->
	<div class="container">
		<div class="row">
			<div class="col-sm-12 col-sm-offset-0">
				<!-- consumption container -->
				<div class="agile_inner_grids">
					<div class="col-md-3 w3ls-markets-grid">
						<div class="agileits-icon-grid">
							<div class="icon-left">
								<i class="fa fa-money" aria-hidden="true"></i>
							</div>
							<div class="icon-right">
								<h5>$0</h5>
								<p>Annual Savings</p>
							</div>
						</div>
					</div>

					<div class="col-md-3 w3ls-markets-grid">
						<div class="agileits-icon-grid">
							<div class="icon-left">
								<i class="fa fa-calendar" aria-hidden="true"></i>
							</div>
							<div class="icon-right">
								<h5>0</h5>
								<p>Payback Period</p>
							</div>
						</div>
					</div>
					<div class="col-md-3 w3ls-markets-grid">
						<div class="agileits-icon-grid">
							<div class="icon-left">
								<i class="fa fa-battery" aria-hidden="true"></i>
							</div>
							<div class="icon-right">
								<h5>$0</h5>
								<p>Battery Cost</p>
							</div>
						</div>
					</div>
					<div class="col-md-3 w3ls-markets-grid">
						<div class="agileits-icon-grid">
							<div class="icon-left">
								<i class="fa fa-signal" aria-hidden="true"></i>
							</div>
							<div class="icon-right">
								<h5>0%</h5>
								<p>Return on Investment</p>
							</div>
						</div>
					</div>
					<div class="clearfix"></div>
				</div>

				<!-- Load bar chart -->
				<div class="ct-chart" id="bar-chart"></div>

				<div class="main-solar">

					<div class="list-solar" style="height: 169px;">
						<div class="box-solar">
							<h3>SOLAR BATTERY</h3>
							<ul>
								<li>
									<p>
										Battery capacity<span id="Batterycapacity"><c:out
												value="${powerConsumptionVO.getBatteryCapacity()}" /><i
											class="fw200">Ah</i></span>
									</p>
								</li>
								<li>
									<p>
										Solar Charger Capacity<span id="Solarbatterycost"><c:out
												value="${powerConsumptionVO.getSolarChargerCapacity()}" />
											<i class="fw200">A</i> </span>
									</p>
								</li>
								<li>
									<p>
										PV Sizing<span id="PVSizing"><c:out
												value="${powerConsumptionVO.getPvModuleSize()}" /></span>
									</p>
								</li>
							</ul>
						</div>
					</div>
					<div class="list-solar" style="height: 169px;">
						<div class="box-solar">
							<h3>Your electricity</h3>
							<ul>
								<li>
									<p>
										Battery powered<span id="Batterypowered">0.0 <i
											class="fw200">kW</i></span>
									</p>
								</li>
								<li>
									<p>
										Solar powered<span id="Solarpowered">0.0 <i
											class="fw200">kW</i></span>
									</p>
								</li>
								<li>
									<p>
										Fed back into gird<span id="fbgrid">0.00 <i
											class="fw200">kW</i></span>
									</p>
								</li>
							</ul>
						</div>
					</div>
					<div class="list-solar" style="width: 100%; height: 200px;">
						<div class="box-solar">
							<h3 style="border-bottom: 6px solid #bd110c;">Battery
								savings</h3>
							<ul>
								<li>
									<p>
										Annual battery savings<span id="abs">$0</span>
									</p>
								</li>
								<li>
									<p>
										Lifetime battery savings<span id="lbs">$0.0</span>
									</p>
								</li>
								<li>
									<p>
										Payback period<span id="pb">0.0 years</span>
									</p>
								</li>
								<li>
									<p>
										Return on investment<span id="ri">0.0%</span>
									</p>
								</li>
							</ul>
						</div>
					</div>
				</div>
				<!-- consumption container -->
			</div>
		</div>
		<!-- row -->
	</div>
	<!-- //calculations -->
</body>

<footer>
	<%@ include file="template/footer.jsp"%>
</footer>

</html>

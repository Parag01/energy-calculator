<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!doctype html>
<html lang="en">
<head>
<%@ include file="template/header.jsp"%>
</head>

<body>
	<!-- banner -->
	<div class="banner">
		<div class="container">
			<nav class="navbar navbar-default">
				<div class="navbar-header navbar-left">
					<button type="button" class="navbar-toggle collapsed"
						data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
						<span class="sr-only">Toggle navigation</span> <span
							class="icon-bar"></span> <span class="icon-bar"></span> <span
							class="icon-bar"></span>
					</button>
					<h1>
						<a class="navbar-brand" href="index.html"><span>R</span>adiant
							Energy</a>
					</h1>
				</div>
				<!-- Collect the nav links, forms, and other content for toggling -->
				<div class="agileits_w3layouts_sign_in">
					<a href="solarquote" class="play-icon popup-with-zoom-anim">Solar
						Quotes</a>

				</div>
				<div class="collapse navbar-collapse navbar-right"
					id="bs-example-navbar-collapse-1">
					<nav class="menu--puck">
						<ul class="menu__list">
							<li class="menu__item menu__item--current"><a
								href="index" class="menu__link">Home</a></li>
							<li class="menu__item"><a href="about" class="menu__link">About</a></li>
							<li class="menu__item"><a href="projects" class="menu__link">Projects</a></li>
							<li class="menu__item"><a href="contact" class="menu__link">Contact</a></li>
						</ul>
					</nav>

				</div>
				<div class="clearfix"></div>
			</nav>
			<div class="w3layouts_banner_info">
				<!-- banner-text -->
				<div class="slider">
					<div class="callbacks_container">
						<div class="agileits-banner-info">
							<h3>ENERGY FOR LIVING</h3>
							<p>Plug into the sun!</p>
						</div>
					</div>

				</div>
				<!-- //banner-text -->
				<div class="wthree_banner_grids">
					<div class="col-md-3 wthree_banner_grid bottom-head">
						<a href="#">
							<div class="buy-media">
								<i class="fa fa-pagelines" aria-hidden="true"></i>
								<h4>Green Energy</h4>
							</div>
						</a>
					</div>
					<div class="col-md-3 wthree_banner_grid bottom-head">
						<a href="#">
							<div class="buy-media">
								<i class="fa fa-tint" aria-hidden="true"></i>
								<h4>Water Purification</h4>
							</div>
						</a>
					</div>
					<div class="col-md-3 wthree_banner_grid bottom-head">
						<a href="#">
							<div class="buy-media">
								<i class="fa fa-plug" aria-hidden="true"></i>
								<h4>Natural Power</h4>
							</div>
						</a>
					</div>
					<div class="col-md-3 wthree_banner_grid bottom-head">
						<a href="#">
							<div class="buy-media">
								<i class="fa fa-bolt" aria-hidden="true"></i>
								<h4>Recycling</h4>
							</div>
						</a>
					</div>
					<div class="clearfix"></div>
				</div>

			</div>
		</div>
	</div>
	<!-- //banner -->
	<!-- banner-bottom -->
	<div id="about" class="banner-bottom">
		<div class="container">
			<h2>
				<span>Welcome to our </span> Radiant Energy
			</h2>
			<p class="agile_des">Lorem ipsum dolor sit amet, consectetur
				adipiscing elit. Phasellus at placerat ante. Praesent nulla nunc,
				pretium dapibus efficitur in, auctor eget elit. Lorem ipsum dolor
				sit amet fusce eget erat nunc..</p>
			<div class="agile_inner_grids">
				<div class="col-md-6 w3layouts_statistics_grid_right">
					<h3 class="wthree_text_info">
						About <span>Us </span>
					</h3>
					<h4>Nulla faucibus mauris ac leo imperdiet, id auctor urna
						consectetur</h4>
					<p>Ut laoreet turpis tristique, sollicitudin eros nec,
						scelerisque magna. Integer porttitor diam orci, id feugiat urna
						auctor a. Donec aliquet ante ut lacinia placerat.Sed gravida
						tristique varius.</p>
					<div class="col-md-4 w3_stats_grid">
						<h3 id="w3l_stats1" class="odometer">0</h3>
						<p>Branches</p>
						<div class="w3layouts_stats_grid1">
							<i class="fa fa-building-o" aria-hidden="true"></i>
						</div>
					</div>
					<div class="col-md-4 w3_stats_grid">
						<h3 id="w3l_stats2" class="odometer">0</h3>
						<p>Projects</p>
						<div class="w3layouts_stats_grid1">
							<i class="fa fa-clone" aria-hidden="true"></i>
						</div>
					</div>
					<div class="col-md-4 w3_stats_grid">
						<h3 id="w3l_stats3" class="odometer">0</h3>
						<p>Workers</p>
						<div class="w3layouts_stats_grid1">
							<i class="fa fa-users" aria-hidden="true"></i>
						</div>
					</div>
				</div>
				<div class="col-md-6 w3layouts_about_grid_left"></div>
				<div class="clearfix"></div>
			</div>
		</div>
	</div>
	<!-- //banner-bottom -->
	<div class="news_agile">
		<div class="col-md-6 agile_info_news">
			<div class="news_info_img grid">
				<figure class="effect-ruby">
					<a href="#" data-toggle="modal" data-target="#myModal1"><img
						src="${g6}" alt=" " class="img-responsive"></a>
					<figcaption>
						<h3>
							Radiant <span>Energy</span>
						</h3>
						<p>PLUG INTO THE SUN!</p>
					</figcaption>
				</figure>
			</div>
		</div>
		<div class="col-md-6 agile_info_text">
			<h4>
				<a href="#" data-toggle="modal" data-target="#myModal1">New
					Equipments</a>
			</h4>
			<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.
				Phasellus at placerat ante. Praesent nulla nunc, pretium dapibus
				efficitur .Ut laoreet turpis tristique, sollicitudin eros nec,
				scelerisque magna. Integer porttitor diam orci.</p>
			<ul class="new_w3ls_agile">
				<li><a href="#"><i class="fa fa-user" aria-hidden="true"></i>
						Admin</a></li>
				<li><i class="fa fa-calendar" aria-hidden="true"></i> May 18th,
					2017</li>
				<li><a href="#"><i class="fa fa-comment-o"
						aria-hidden="true"></i> Comments (2)</a></li>
			</ul>
			<a class="learn" href="#" data-toggle="modal" data-target="#myModal1">Learn
				More</a>
		</div>
		<div class="clearfix"></div>
		<div class="col-md-6 agile_info_text">
			<h4>
				<a href="#" data-toggle="modal" data-target="#myModal1">Power
					and Energy</a>
			</h4>
			<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.
				Phasellus at placerat ante. Praesent nulla nunc, pretium dapibus
				efficitur .Ut laoreet turpis tristique, sollicitudin eros nec,
				scelerisque magna. Integer porttitor diam orci.</p>
			<ul class="new_w3ls_agile">
				<li><a href="#"><i class="fa fa-user" aria-hidden="true"></i>
						Admin</a></li>
				<li><i class="fa fa-calendar" aria-hidden="true"></i> May 18th,
					2017</li>
				<li><a href="#"><i class="fa fa-comment-o"
						aria-hidden="true"></i> Comments (2)</a></li>
			</ul>
			<a class="learn" href="#" data-toggle="modal" data-target="#myModal1">Learn
				More</a>
		</div>
		<div class="col-md-6 agile_info_news">
			<div class="news_info_img grid">
				<figure class="effect-ruby">
					<a href="#" data-toggle="modal" data-target="#myModal1"><img
						src="${g1}" alt=" " class="img-responsive"></a>
					<figcaption>
						<h3>
							Radiant <span>Energy</span>
						</h3>
						<p>PLUG INTO THE SUN!</p>
					</figcaption>
				</figure>
			</div>
		</div>
		<div class="clearfix"></div>
	</div>
	<!-- /middle -->
	<div class="w3layouts_bottom">
		<div class="container">
			<div class="col-md-9 w3layouts_getin_info">
				<h3>Are Looking for Solar Solutions ?</h3>
			</div>
			<div class="col-md-3 w3layouts_getin">
				<a href="contact.html">Get In Touch</a>
			</div>
			<div class="clearfix"></div>
		</div>
	</div>
	<!-- //middle -->
	<!-- /projects -->
	<div class="portfolio">
		<ul id="flexiselDemo1">

			<li>
				<div class="agileinfo_portfolio_grid">
					<a href="projects.html">
						<div class="agileits_portfolio_grid">
							<img src="${g5}" alt=" " class="img-responsive" />
						</div>
					</a>
				</div>
			</li>
			<li>
				<div class="agileinfo_portfolio_grid">
					<a href="projects.html">
						<div class="agileits_portfolio_grid">
							<img src="${g4}" alt=" " class="img-responsive" />
						</div>
					</a>
				</div>
			</li>
			<li>
				<div class="agileinfo_portfolio_grid">
					<a href="projects.html">
						<div class="agileits_portfolio_grid">
							<img src="${g3}" alt=" " class="img-responsive" />
						</div>
					</a>
				</div>
			</li>
			<li>
				<div class="agileinfo_portfolio_grid">
					<a href="projects.html">
						<div class="agileits_portfolio_grid">
							<img src="${g1}" alt=" " class="img-responsive" />
						</div>
					</a>
				</div>

			</li>
			<li>
				<div class="agileinfo_portfolio_grid">
					<a href="projects.html">
						<div class="agileits_portfolio_grid">
							<img src="${g6}" alt=" " class="img-responsive" />
						</div>
					</a>
				</div>
			</li>
		</ul>
	</div>
	<!-- //projects -->
	<div class="agile_services">
		<div class="container">
			<h3 class="wthree_text_info two">
				Some <span>Real Facts</span>
			</h3>
			<div class="agile_inner_grids">
				<div
					class="col-md-6 agile_text_info_bar w3layouts_statistics_grid_right">

					<h4>We are passionate about our work</h4>
					<p>Ut laoreet turpis tristique, sollicitudin eros nec,
						scelerisque magna. Integer porttitor diam orci, id feugiat urna
						auctor a. Donec aliquet ante ut lacinia placerat.Sed gravida
						tristique varius.</p>
					<img src="${bar}" alt=" " class="img-responsive" />
				</div>
				<div class="col-md-6 agile_info_bar">

					<div id="chart">
						<ul id="numbers">
							<li><span>100%</span></li>
							<li><span>90%</span></li>
							<li><span>80%</span></li>
							<li><span>70%</span></li>
							<li><span>60%</span></li>
							<li><span>50%</span></li>
							<li><span>40%</span></li>
							<li><span>30%</span></li>
							<li><span>20%</span></li>
							<li><span>10%</span></li>
							<li><span>0%</span></li>
						</ul>
						<ul id="bars">
							<li>
								<div data-percentage="56" class="bar"></div> <span>Option
									1</span>
							</li>
							<li>
								<div data-percentage="33" class="bar"></div> <span>Option
									2</span>
							</li>
							<li>
								<div data-percentage="54" class="bar"></div> <span>Option
									3</span>
							</li>
							<li>
								<div data-percentage="94" class="bar"></div> <span>Option
									4</span>
							</li>
							<li>
								<div data-percentage="44" class="bar"></div> <span>Option
									5</span>
							</li>
							<li>
								<div data-percentage="23" class="bar"></div> <span>Option
									6</span>
							</li>
						</ul>
					</div>
				</div>
				<div class="clearfix"></div>
			</div>
		</div>
	</div>
	<!-- Modal1 -->
	<div class="modal fade" id="myModal1" tabindex="-1" role="dialog">
		<div class="modal-dialog">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<div class="agileits_w3layouts_heding">
						<h3>
							Radiant <span>Energy</span>
						</h3>
						<img src="${banner1}" alt="" />
						<p>Ut laoreet turpis tristique, sollicitudin eros nec,
							scelerisque magna. Integer porttitor diam orci, id feugiat urna
							auctor a. Donec aliquet ante ut lacinia placerat.Sed gravida
							tristique varius.</p>
					</div>

					<div class="clearfix"></div>
				</div>
			</div>
		</div>
	</div>
	<!-- //Modal1 -->
</body>

<footer>
	<%@ include file="template/footer.jsp"%>
</footer>

</html>

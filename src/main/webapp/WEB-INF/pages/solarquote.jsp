<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!doctype html>
<html lang="en">
<head>
<%@ include file="template/header.jsp"%>
</head>

<body>
	<!-- banner -->
	<div class="banner_inner_agileits">
		<div class="container">
			<nav class="navbar navbar-default">
				<div class="navbar-header navbar-left">
					<button type="button" class="navbar-toggle collapsed"
						data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
						<span class="sr-only">Toggle navigation</span> <span
							class="icon-bar"></span> <span class="icon-bar"></span> <span
							class="icon-bar"></span>
					</button>
					<h1>
						<a class="navbar-brand" href="index.html"><span>R</span>adiant
							Energy</a>
					</h1>
				</div>
				<!-- Collect the nav links, forms, and other content for toggling -->
				<div class="agileits_w3layouts_sign_in">
					<a href="solarquote" class="play-icon popup-with-zoom-anim">Solar
						Quotes</a>

				</div>
				<div class="collapse navbar-collapse navbar-right"
					id="bs-example-navbar-collapse-1">
					<nav class="menu--puck">
						<ul class="menu__list">
							<li class="menu__item"><a href="index" class="menu__link">Home</a></li>
							<li class="menu__item"><a href="about" class="menu__link">About</a></li>
							<li class="menu__item"><a href="projects" class="menu__link">Projects</a></li>
							<li class="menu__item"><a href="contact" class="menu__link">Contact</a></li>
						</ul>
					</nav>

				</div>
				<div class="clearfix"></div>
			</nav>
			<div class="w3layouts_banner_info_inner">
				<h2>
					Solar <span>Quote</span>
				</h2>
				<p>Calculate the Solar Energy Consumption</p>
			</div>
		</div>
	</div>
	<!-- //banner -->
	<!-- calculations -->
	<div class="container">
		<div class="row">
			<div class="col-sm-8 col-sm-offset-2">
				<!-- wizard container -->
				<div class="wizard-container">
					<div class="card wizard-card" data-color="red" id="wizard">
						<form:form id="solarCalculator" modelAttribute="solarEntityVO"
							action="caculate" method="post">

							<div class="wizard-header">
								<h3 class="wizard-title">SOLAR STORAGE CALCULATOR</h3>
								<h5>This information will let us know more about you.</h5>
							</div>
							<div class="wizard-navigation">
								<ul>
									<li><a href="#household" data-toggle="tab">Household
											Details</a></li>
									<li><a href="#property" data-toggle="tab">Property
											Details</a></li>
									<li><a href="#finance" data-toggle="tab">Finance
											Options</a></li>
								</ul>
							</div>

							<div class="tab-content">
								<div class="tab-pane" id="household">
									<div class="row">
										<div class="col-sm-12">
											<h4 class="info-text">Let's start with the basic
												details.</h4>
										</div>
										<div class="col-sm-6">
											<div class="input-group">
												<span class="input-group-addon"> <i
													class="material-icons">home</i>
												</span>
												<div class="form-group label-floating">
													<label class="control-label">Postcode</label>
													<form:input id="postcode" name="postcode" path="postcode"
														type="text" class="form-control" />
												</div>
											</div>

											<div class="input-group">
												<span class="input-group-addon"> <i
													class="material-icons">av_timer</i>
												</span>
												<div class="form-group label-floating">
													<label class="control-label">Electricity (daily)</label>
													<form:input id="electricity" name="electricity"
														path="electricity" type="text" class="form-control" />
												</div>
											</div>

											<div class="input-group">
												<span class="input-group-addon"> <i
													class="material-icons">av_timer</i>
												</span>
												<div class="form-group label-floating">
													<label class="control-label">PV Modules Capacity</label>
													<form:select path="pvModules" id="pvModules"
														name="pvModules" class="form-control">
														<option value="110">110</option>
														<option value="260">260</option>
													</form:select>
												</div>
											</div>

											<div class="input-group">
												<span class="input-group-addon"> <i
													class="material-icons">battery_charging_full</i>
												</span>
												<div class="form-group label-floating">
													<label class="control-label">Nominal Battery
														Voltage (12 or 24)</label>
													<form:select path="batteryVoltage" id="batteryVoltage"
														name="batteryVoltage" class="form-control">
														<option value="12">12</option>
														<option value="24">24</option>
													</form:select>
												</div>
											</div>
										</div>
										<div class="col-sm-6">
											<div class="input-group">
												<span class="input-group-addon"> <i
													class="material-icons">settings_system_daydream</i>
												</span>
												<div class="form-group label-floating">
													<label class="control-label">System Size (1.5KW -
														10KW)</label>
													<form:input id="systemSize" name="systemSize"
														path="systemSize" type="text" class="form-control" />
												</div>
											</div>

											<div class="input-group">
												<span class="input-group-addon"> <i
													class="material-icons">battery_full</i>
												</span>
												<div class="form-group label-floating">
													<label class="control-label">Electricity
														consumption % between 8am - 6pm</label>
													<form:input id="electricityConsumption"
														name="electricityConsumption"
														path="electricityConsumption" type="text"
														class="form-control" />
												</div>
											</div>

											<div class="input-group">
												<span class="input-group-addon"> <i
													class="material-icons">battery_unknown</i>
												</span>
												<div class="form-group label-floating">
													<label class="control-label">Battery Cost</label>
													<form:input id="batteryCost" name="batteryCost"
														path="batteryCost" type="text" class="form-control" />
												</div>
											</div>

											<div class="input-group">
												<span class="input-group-addon"> <i
													class="material-icons">today</i>
												</span>
												<div class="form-group label-floating">
													<label class="control-label">Days of Autonomy</label>
													<form:input id="daysOfAutonomy" name="daysOfAutonomy"
														path="daysOfAutonomy" type="text" class="form-control" />
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="tab-pane" id="property">
									<h4 class="info-text">Is your roof subject to shading or
										obstruction?</h4>
									<div class="row">
										<div class="col-sm-10 col-sm-offset-1">
											<div class="col-sm-4">
												<div class="choice" data-toggle="wizard-radio" rel="tooltip"
													title="Select 0 hours.">
													<input type="radio" name="job" value="Design">
													<div class="icon">
														<i class="material-icons">timelapse</i>
													</div>
													<h6>0 Hours</h6>
												</div>
											</div>
											<div class="col-sm-4">
												<div class="choice" data-toggle="wizard-radio" rel="tooltip"
													title="Select 0.5 hours.">
													<input type="radio" name="job" value="Code">
													<div class="icon">
														<i class="material-icons">access_time</i>
													</div>
													<h6>0.5 Hours</h6>
												</div>
											</div>
											<div class="col-sm-4">
												<div class="choice" data-toggle="wizard-radio" rel="tooltip"
													title="Select 1 hours.">
													<input type="radio" name="job" value="Code">
													<div class="icon">
														<i class="material-icons">timelapse</i>
													</div>
													<h6>1 Hours</h6>
												</div>
											</div>
										</div>
										<div class="col-sm-10 col-sm-offset-1">
											<div class="col-sm-4">
												<div class="choice" data-toggle="wizard-radio" rel="tooltip"
													title="Select 1.5 hours.">
													<input type="radio" name="job" value="Design">
													<div class="icon">
														<i class="material-icons">access_time</i>
													</div>
													<h6>1.5 Hours</h6>
												</div>
											</div>
											<div class="col-sm-4">
												<div class="choice" data-toggle="wizard-radio" rel="tooltip"
													title="Select 2 hours.">
													<input type="radio" name="job" value="Code">
													<div class="icon">
														<i class="material-icons">timelapse</i>
													</div>
													<h6>2 Hours</h6>
												</div>
											</div>
											<div class="col-sm-4">
												<div class="choice" data-toggle="wizard-radio" rel="tooltip"
													title="Select 2.5 hours.">
													<input type="radio" name="job" value="Code">
													<div class="icon">
														<i class="material-icons">access_time</i>
													</div>
													<h6>2.5 Hours</h6>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="tab-pane" id="finance">
									<div class="row">
										<div class="col-sm-10 col-sm-offset-1">
											<div class="col-sm-4">
												<div class="choice" data-toggle="wizard-radio" rel="tooltip"
													title="No loan or mortgage.">
													<input type="radio" name="job" value="Design">
													<div class="icon">
														<i class="material-icons">attach_money</i>
													</div>
													<h6>Pay Cash upfront</h6>
												</div>
											</div>
											<div class="col-sm-4">
												<div class="choice" data-toggle="wizard-radio" rel="tooltip"
													title="Add to mortgage.">
													<input type="radio" name="job" value="Code">
													<div class="icon">
														<i class="material-icons">monetization_on</i>
													</div>
													<h6>0% upfront (Add to mortgage)</h6>
												</div>
											</div>
											<div class="col-sm-4">
												<div class="choice" data-toggle="wizard-radio" rel="tooltip"
													title="Get solar loan.">
													<input type="radio" name="job" value="Code">
													<div class="icon">
														<i class="material-icons">monetization_on</i>
													</div>
													<h6>0% upfront (Get loan)</h6>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="wizard-footer">
								<div class="pull-right">
									<input type='button'
										class='btn btn-next btn-fill btn-danger btn-wd' name='next'
										value='Next' /> <input
										class='btn btn-finish btn-fill btn-danger btn-wd'
										name='submit' type="submit" value='Submit' />
								</div>
								<div class="pull-left">
									<input type='button'
										class='btn btn-previous btn-fill btn-default btn-wd'
										name='previous' value='Previous' />
								</div>
								<div class="clearfix"></div>
							</div>
						</form:form>
					</div>
				</div>
				<!-- wizard container -->
			</div>
		</div>
		<!-- row -->
	</div>
	<!-- //calculations -->
</body>

<footer>
	<%@ include file="template/footer.jsp"%>
</footer>

</html>

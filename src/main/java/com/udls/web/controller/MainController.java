package com.udls.web.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.udls.bean.PowerConsumptionVO;
import com.udls.bean.SolarEntityVO;
import com.udls.web.model.ConsumptionCalculator;

@Controller
public class MainController {

	@RequestMapping(value = { "/", "/index" }, method = RequestMethod.GET)
	public ModelAndView displayDashboard() {

		ModelAndView model = new ModelAndView("dashboard");

		return model;

	}

	@RequestMapping(value = "/about", method = RequestMethod.GET)
	public ModelAndView displayAbout() {

		ModelAndView model = new ModelAndView("about");

		return model;

	}

	@RequestMapping(value = "/projects", method = RequestMethod.GET)
	public ModelAndView displayProjects() {

		ModelAndView model = new ModelAndView("projects");

		return model;

	}

	@RequestMapping(value = "/contact", method = RequestMethod.GET)
	public ModelAndView displayContact() {

		ModelAndView model = new ModelAndView("contact");

		return model;

	}

	@RequestMapping(value = "/solarquote", method = RequestMethod.GET)
	public ModelAndView displaySolarQuote() {

		SolarEntityVO solarEntityVO = new SolarEntityVO();

		ModelAndView model = new ModelAndView("solarquote");
		model.addObject("solarEntityVO", solarEntityVO);

		return model;

	}

	@RequestMapping(value = "/caculate", method = RequestMethod.POST)
	public ModelAndView calculateSolarConsumption(HttpServletRequest request, HttpServletResponse response,
			@ModelAttribute("solarEntityVO") @Validated SolarEntityVO solarEntityVO, BindingResult result) {

		ConsumptionCalculator consumptionCalculator = new ConsumptionCalculator();

		PowerConsumptionVO powerConsumptionVO = new PowerConsumptionVO();
		powerConsumptionVO = consumptionCalculator.calculateConsumption(solarEntityVO);

		ModelAndView model = new ModelAndView("consumption");
		model.addObject("powerConsumptionVO", powerConsumptionVO);

		return model;

	}

}
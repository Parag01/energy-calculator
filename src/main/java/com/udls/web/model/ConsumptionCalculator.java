package com.udls.web.model;

import java.util.HashMap;
import java.util.Map;

import com.udls.bean.PowerConsumptionVO;
import com.udls.bean.SolarEntityVO;

public class ConsumptionCalculator {

	Map<Integer, Double> map;

	/**
	 * Constructor
	 */
	public ConsumptionCalculator() {
		map = new HashMap<Integer, Double>();
		map.put(110, 7.4);
		map.put(260, 9.0);
	}

	/**
	 * Method to calculate power consumption based on the solar entity input
	 * 
	 * @param solarEntityVO
	 * @return
	 */
	public PowerConsumptionVO calculateConsumption(SolarEntityVO solarEntityVO) {

		PowerConsumptionVO powerConsumptionVO = new PowerConsumptionVO();

		// Calculate the PV Module Size
		powerConsumptionVO.setPvModuleSize(this.calculatePVModuleSize(solarEntityVO));

		// Calculate the Inverter Size
		// powerConsumptionVO.setInverterSizing(this.calculateInverterSize(solarEntityVO));

		// Calculate the Battery Capacity
		powerConsumptionVO.setBatteryCapacity(this.calculateBatteryCapacity(solarEntityVO));

		// Calculate the Solar Charger Capacity
		powerConsumptionVO.setSolarChargerCapacity(
				this.calculateSolarChargerCapacity(solarEntityVO, powerConsumptionVO.getPvModuleSize()));

		return powerConsumptionVO;
	}

	/**
	 * Method to calculate PV Module Size
	 * 
	 * @return
	 */
	private int calculatePVModuleSize(SolarEntityVO solarEntityVO) {

		double result;

		result = Integer.parseInt(solarEntityVO.getElectricityConsumption()) / 30;
		result *= 1.3;
		result /= 4;
		result /= Integer.parseInt(solarEntityVO.getPvModules());

		return (int) Math.ceil(result);
	}

	/**
	 * Method to calculate Inverter Size
	 * 
	 * @param solarEntityVO
	 * 
	 * @return
	 */
	@SuppressWarnings("unused")
	private int calculateInverterSize(SolarEntityVO solarEntityVO) {
		return 0;
	}

	/**
	 * Method to calculate Battery Capacity
	 * 
	 * @param solarEntityVO
	 * 
	 * @return
	 */
	private int calculateBatteryCapacity(SolarEntityVO solarEntityVO) {

		double result;

		result = Integer.parseInt(solarEntityVO.getElectricityConsumption()) / 30;
		result /= (0.85 * 0.6 * Integer.parseInt(solarEntityVO.getBatteryVoltage()));
		result *= solarEntityVO.getDaysOfAutonomy();

		return (int) result;
	}

	/**
	 * Method to calculate Solar Charger Capacity
	 * 
	 * @param solarEntityVO
	 * @param i
	 * 
	 * @return
	 */
	private int calculateSolarChargerCapacity(SolarEntityVO solarEntityVO, int pvModuleSize) {

		double result = 0;

		result = pvModuleSize * this.map.get(Integer.parseInt(solarEntityVO.getPvModules())) * 1.3;

		return (int) result;
	}

}

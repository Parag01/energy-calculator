package com.udls.bean;

public class SolarEntityVO {

	private String postcode;
	private String electricity;
	private String systemSize;
	private String electricityConsumption;
	private String batteryVoltage;
	private String batteryCost;
	private String roofObstruction;
	private String financeMethod;
	private String pvModules;
	private int daysOfAutonomy;

	public String getPostcode() {
		return postcode;
	}

	public void setPostcode(String postcode) {
		this.postcode = postcode;
	}

	public String getElectricity() {
		return electricity;
	}

	public void setElectricity(String electricity) {
		this.electricity = electricity;
	}

	public String getSystemSize() {
		return systemSize;
	}

	public void setSystemSize(String systemSize) {
		this.systemSize = systemSize;
	}

	public String getElectricityConsumption() {
		return electricityConsumption;
	}

	public void setElectricityConsumption(String electricityConsumption) {
		this.electricityConsumption = electricityConsumption;
	}

	public String getBatteryVoltage() {
		return batteryVoltage;
	}

	public void setBatteryVoltage(String batteryVoltage) {
		this.batteryVoltage = batteryVoltage;
	}

	public String getBatteryCost() {
		return batteryCost;
	}

	public void setBatteryCost(String batteryCost) {
		this.batteryCost = batteryCost;
	}

	public String getRoofObstruction() {
		return roofObstruction;
	}

	public void setRoofObstruction(String roofObstruction) {
		this.roofObstruction = roofObstruction;
	}

	public String getFinanceMethod() {
		return financeMethod;
	}

	public void setFinanceMethod(String financeMethod) {
		this.financeMethod = financeMethod;
	}

	public String getPvModules() {
		return pvModules;
	}

	public void setPvModules(String pvModules) {
		this.pvModules = pvModules;
	}

	public int getDaysOfAutonomy() {
		return daysOfAutonomy;
	}

	public void setDaysOfAutonomy(int daysOfAutonomy) {
		this.daysOfAutonomy = daysOfAutonomy;
	}

}

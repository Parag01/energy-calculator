package com.udls.bean;

public class PowerConsumptionVO {

	private int pvModuleSize;
	private int inverterSizing;
	private int batteryCapacity;
	private int solarChargerCapacity;

	public int getPvModuleSize() {
		return pvModuleSize;
	}

	public void setPvModuleSize(int pvModuleSize) {
		this.pvModuleSize = pvModuleSize;
	}

	public int getInverterSizing() {
		return inverterSizing;
	}

	public void setInverterSizing(int inverterSizing) {
		this.inverterSizing = inverterSizing;
	}

	public int getBatteryCapacity() {
		return batteryCapacity;
	}

	public void setBatteryCapacity(int batteryCapacity) {
		this.batteryCapacity = batteryCapacity;
	}

	public int getSolarChargerCapacity() {
		return solarChargerCapacity;
	}

	public void setSolarChargerCapacity(int solarChargerCapacity) {
		this.solarChargerCapacity = solarChargerCapacity;
	}

}
